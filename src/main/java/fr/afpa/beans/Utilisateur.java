package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_utilisateur")
	private int idUser;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;
	@NonNull
	private String eMail;
	@NonNull
	private String numTel;
	@NonNull
	private String nomLibrairie;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_adresse")
	@NonNull
	private Adresse adresse;

	@OneToMany(mappedBy = "utilisateur", fetch = FetchType.EAGER)
	private List<Annonce> listeAnnonce;

	/**
	 * 
	 */
	public String toString() {
		return "infos:\n" 
			 + "       >> nom :" + nom + "\n" 
			 + "       >> prenom :" + prenom + "\n"
			 + "       >> nomLibrairie :" + nomLibrairie + "\n" 
			 + "       >> email :" + eMail + "\n"
			 + "       >> numero telephone :" + numTel + "\n";
	}
}
