package fr.afpa.beans;

import java.text.DecimalFormat;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "findByTitre", query = "from Annonce u where u.titre like :motCle")
@NamedQuery(name = "findByISBN", query = "from Annonce u where u.isbn = :isbn")
@NamedQuery(name = "findByVille", query = "from Annonce u where u.utilisateur.adresse.ville = :ville")
public final class Annonce {

	public Annonce(String maisonEdition, String titre, LocalDate dateEdition, String isbn, float prixUnitaire,
			int quantite, float remise, Utilisateur utilisateur) {
		
		this.maisonEdition = maisonEdition;
		this.titre = titre;
		this.dateEdition = dateEdition;
		this.isbn = isbn;
		this.prixUnitaire = prixUnitaire;
		this.quantite = quantite;
		this.remise = remise;
		this.utilisateur = utilisateur;
		this.dateAnnonce = LocalDate.now();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_annonce")
	private int idAnnonce;

	private String maisonEdition;

	private String titre;

	private LocalDate dateAnnonce;

	private LocalDate dateEdition;

	private String isbn;

	private float prixUnitaire;

	private int quantite;

	private float remise;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_utilisateur")
	private Utilisateur utilisateur;

	/**
	 * 
	 */
	public String toString() {
		DecimalFormat df = new DecimalFormat(".##");
		return "   ID ANNONCE : " + idAnnonce + "\n" 
				+ "   DATE-ANNONCE : " + dateAnnonce + "\n" 
		        + "   TITRE : " + titre+ "\n" 
				+ "   MAISON-EDITION : " + maisonEdition + "\n" 
		        + "   DATE-EDITION : " + dateEdition + "\n"
				+ "   ISBN : " + isbn + "\n" 
		        + "   PRIX-UNITAIRE : " + prixUnitaire + " euros\n" 
				+ "   QUANTITE : "+ quantite + "\n" 
		        + "   REMISE : " + remise + "%\n" 
				+ "   PRIX-TOTALE : "+ df.format(prixUnitaire * quantite * (1 - (remise / 100))) + " euros";
	}
}
