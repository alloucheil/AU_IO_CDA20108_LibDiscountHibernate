package fr.afpa.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LibDiscount
 *
 */
public class UtilisateurDAO {

	private static Session session = null;

	/**
	 * Pour ajouter un nouveau utilisateur � la base de donn�es
	 * 
	 * @param utilisateur
	 * @return l'utilisateur avec son id g�n�r�
	 */
	public void addUtilisateur(Utilisateur utilisateur) {

		session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.persist(utilisateur);

		tx.commit();
		session.close();

	}

	/**
	 * Pour mettre � jour les infos d'un utilisateur dans la base de donn�es
	 * 
	 * @param utilisateur
	 */
	public void updateUtilisateur(Utilisateur utilisateur) {
		session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.update(utilisateur);

		tx.commit();
		session.close();
	}

}
