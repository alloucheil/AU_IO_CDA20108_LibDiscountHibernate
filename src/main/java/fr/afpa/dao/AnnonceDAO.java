package fr.afpa.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author CDA
 *
 */
public class AnnonceDAO {

	/**
	 * Pour ajouter une annonce � la base de donn�es
	 * 
	 * @param annonce
	 * @return
	 */
	public void addAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.update(annonce.getUtilisateur());
		session.persist(annonce);

		tx.commit();
		session.close();
	}

	/**
	 * Pour mettre � jour les infos d'une annonce dans la base de donn�es
	 * 
	 * @param annonce
	 */
	public void updateAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.update(annonce);

		tx.commit();
		session.close();
	}

	/**
	 * Pour supprimer une annonce de la base de donn�es en fonction de son
	 * id_annonce
	 * 
	 * @param idAnnonce
	 */
	public void supprimerAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.delete(annonce);

		tx.commit();
		session.close();
	}

	/**
	 * Pour lister toutes les annonces enregistr�es dans la base de donn�es
	 * 
	 * @return
	 */
	public List<Annonce> listerAnnonce() {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		Query req = session.createQuery("from Annonce");

		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();

		tx.commit();
		session.close();

		return listeAnnonce;

	}

	/**
	 * Pour rechercher et recuperer toutes les annonces contien le mot cle dan le
	 * titre
	 * 
	 * @param motCle
	 * @return
	 */
	public List<Annonce> searchAnnonceParTitre(String titre) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("findByTitre");
		req.setParameter("motCle", "%" + titre + "%");

		@SuppressWarnings("unchecked")
		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();

		tx.commit();
		session.close();

		return listeAnnonce;
	}

	/**
	 * Pour rechercher un annonce en foction de son ISBN
	 * 
	 * @param isbn
	 * @return
	 */
	public Annonce searchAnnonceParISBN(String isbn) {

		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("findByISBN");
		req.setParameter("isbn", isbn);

		Annonce annonce = req.getResultList().isEmpty() ? null : (Annonce) req.getResultList().get(0);

		tx.commit();
		session.close();

		return annonce;

	}

	/**
	 * Pour recuperer toutes les annonce poster dans une ville donn�e
	 * 
	 * @param ville
	 * @return
	 */
	public List<Annonce> searchAnnonceParVille(String ville) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("findByVille");
		req.setParameter("ville", ville);

		@SuppressWarnings("unchecked")
		List<Annonce> annonce = req.getResultList();

		tx.commit();
		session.close();

		return annonce;
	}

}
