package fr.afpa.controles;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.AuthMetier;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthControle {

	/**
	 * Pour verifier le format de LOGIN
	 * 
	 * @param login
	 * @return
	 */
	public boolean checkLogin(String login) {
		return login != null && login.length() >= 5 && login.matches("[0-9a-zA-Z]+");
	}

	/**
	 * Pour verifier le format de PASSWORD
	 * 
	 * @param login
	 * @return
	 */
	public boolean checkPassword(String password) {
		return password != null && password.length() >= 8;
	}

	/**
	 * Pour recupperer l'auth d'un utilisateur en fonction de son idUser
	 * 
	 * @param idUser
	 * @return
	 */
	public Auth getAuth(Utilisateur user) {
		return new AuthMetier().getAuth(user);
	}

	public Auth getAuth(String login, String password) {
		return new AuthMetier().getAuth(login, password);
	}

	public void addAuth(String login, String password, Utilisateur utilisateur) {
		new AuthMetier().addAuth(login,password,utilisateur);
		
	}

	public void updateAuth(Auth auth) {
		new AuthMetier().updateAuth(auth);
		
	}

//	/**
//	 * Pour verifier si le LOGIN et le PASSWORD sont correctes
//	 * 
//	 * @param login
//	 * @param password
//	 * @return
//	 */
//	public int checkAuth(String login, String password) {
//		return new AuthMetier().checkAuth(new Auth(login, password));
//	}

}
